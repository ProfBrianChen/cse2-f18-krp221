// Kristine Phan
// 9/20/18
// CSE 2 311
// write a program that will pick a random card from the deck so you can practice your tricks 
//  random number generator to select a number from 1 to 52 (inclusive).  
// 1-13 = diamonds, 14-26 = clubs, 27-39 = hearts, 40-52 = spades, ascending with  card number:
// 14 is the ace of clubs, 15 is the 2 of clubs, and 26 is the king of clubs
public class CardGenerator{
  public static void main (String[] args){

    int randomNumber = (int) (Math.random() * 52)+1; // Create range of random numbers from 1 to 52
    String suit; // Create string variable of what type of suit the random number belongs to
    
    if ((randomNumber >= 1) && (randomNumber<=13)) {
     suit = "Diamonds"; // Assign suit of diamonds for numbers 1-13
    }
     else if ((randomNumber > 13) && (randomNumber <= 26)) {
      suit = "Clubs"; // Assign suit of clubs for numbers 14-26
     }
      
     else if ((randomNumber > 26) && (randomNumber <= 39)) {
      suit = "Hearts"; // Assign suit of clubs for numbers 27-39
     }
     else  {
      suit = "Spades"; // Assign suit of clubs for numbers 40-52
     }
       
    switch (randomNumber % 13){
    case 1: // if there's a remainder of 1 (#s 1, 14, 27, 40) print out that it is an ace of whatever suit it was in previous if statement
      System.out.println("You picked the Ace of "+ suit);
      break; //leave to next case
    case 2: // if there's a remainder of 2 (#s 2, 15, 28, 41) print out that it is a 2 of whatever suit it was in previous if
      System.out.println("You picked the 2 of " + suit);
      break; // leave
    case 3:  // if there's a remainder of 3 (#s 3, 16, 29, 42) print out that it is a 3 of whatever suit it was in previous if
      System.out.println("You picked the 3 of " + suit);
      break; // leave
    case 4:  // if there's a remainder of 4 (#s 4, 17, 30, 43) print out that it is a 4 of whatever suit it was in previous if
      System.out.println("You picked the 4 of " + suit);
      break; // leave
    case 5: // if there's a remainder of 5 (#s 5, 18, 31, 44) print out that it is a 5 of whatever suit it was in previous if
      System.out.println("You picked the 5 of " + suit);
      break; // leave
    case 6: // if there's a remainder of 6 (#s 6, 19, 32, 45) print out that it is a 6 of whatever suit it was in previous if
      System.out.println("You picked the 6 of " +suit);
     break; //break
    case 7: // if there's a remainder of 7 (#s 7, 20, 33, 46) print out that it is a 7 of whatever suit it was in previous if
      System.out.println("You picked the 7 of " +suit);
      break; //break
    case 8: // if there's a remainder of 8 (#s 8, 21, 34, 47) print out that it is a 4 of whatever suit it was in previous if
      System.out.println("You picked the 8 of " +suit);
     break; // leave
    case 9: // if there's a remainder of 9 (#s 9, 22, 35, 48) print out that it is a 4 of whatever suit it was in previous if
      System.out.println("You picked the 9 of " + suit);
     break; // leave
    case 10: // if there's a remainder of 10 (#s 10, 23, 36, 49) print out that it is a 4 of whatever suit it was in previous if
      System.out.println("You picked the 10 of " + suit);
     break; // leave
    case 11: // if there's a remainder of 11 (#s 11, 24, 37, 50) print out that it is a 4 of whatever suit it was in previous if
      System.out.println("You picked the Jack of " + suit);
     break; // leave
    case 12: // if there's a remainder of 12 (#s 12, 25, 38, 51) print out that it is a 4 of whatever suit it was in previous if
      System.out.println("You picked the Queen of " + suit);
      break; // leave
    default: // if there's a remainder of 0 (#s 13, 26, 39, 52) print out that it is a 4 of whatever suit it was in previous if
      System.out.println("You picked the King of "  + suit);
     break;
  }
         
    }
  }

