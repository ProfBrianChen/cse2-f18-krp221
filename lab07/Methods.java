// Kristine Phan
// CSE 2 311
// phase 0 -- methods
// We need some basic kinds of words.  We need: 
// 1) Adjectives
// 2) Non-primary nouns appropriate for the subject of a sentence
// 3) Past-tense verbs, and 
// 4) Non-primary nouns appropriate for the object of the sentence.

// Create four methods that correspond to the four sentence components above.  
// Make each method generate a random integer from 0-9, 
// select a random adjective / subject / verb / object from a switch statement, and return the word as a string.
// 1 method for action sentence
  // 1 method for conclusion
  // 1 method to call everything 
 // print everythang

import java.util.Scanner;

public class Methods{

  public static  String adjective(){
  boolean correct = true;
    String val = " ";
    int randomNumber = (int) (Math.random() * 10);
  int adjectiveNum = randomNumber;
  switch(adjectiveNum){
   case 0: val = "funny";
      break;
   case 1: val = "sad";
      break;
   case 2: val = "cool";
            break;
   case 3: val = "great";
            break;
   case 4: val = "nice";
            break;
   case 5: val = "smelly";
            break;
   case 6: val = "happy";
            break;
   case 7: val = "uptight";
            break;
   case 8: val = "awful";
           break;
   case 9: val = "popular";
            break;
  } 
    return val;
  }

  public static String subject(){
    int randomNumber = (int) (Math.random() * 10);
    int subjectNum = randomNumber;
    String val = " ";
    switch(subjectNum){
   case 0: val = "man";
           break;
   case 1: val = "raccoon";
              break;
   case 2: val = "gorilla";
              break;
   case 3: val = "hair tie";
              break;
   case 4: val = "table";
              break;
   case 5: val = "wall";
              break;
   case 6: val = "woman";
              break;
   case 7: val = "goldfish";
              break;
   case 8: val = "laptop";  
              break;
   case 9: val = "eyeball";
              break;
    }  
    return val;
  }
public static String verb(){
  int randomNumber = (int) (Math.random() * 10);
  int verbNum = randomNumber;
  String val = " ";
  switch(verbNum){
  case 0: val = "jumped";
          break;
   case 1: val = "screamed";
            break;
   case 2: val = "stared";
            break;
   case 3: val = "sighed";
            break;
   case 4: val = "smiled";
            break;
   case 5: val = "laughed";
            break;
   case 6: val = "cried";
            break;
   case 7: val = "giggled";
            break;
   case 8: val = "played";  
            break;
   case 9: val = "wrote";
            break;
}
  return val;
  
}
  public static String object(){
    int randomNumber = (int) (Math.random() * 10);
    int objectNum = randomNumber;
    String val = " ";
    switch(objectNum){
   case 0: val = "man";
           break;
   case 1: val = "raccoon";
              break;
   case 2: val = "gorilla";
              break;
   case 3: val = "hair tie";
              break;
   case 4: val = "table";
              break;
   case 5: val = "wall";
              break;
   case 6: val = "woman";
              break;
   case 7: val = "goldfish";
              break;
   case 8: val = "laptop";  
              break;
   case 9: val = "eyeball";
              break;
    }  
    return val;
  }

  public static String thesis(){
  String adjective = adjective(); 
  String subject = subject();
  String  verb = verb();
  String  object = object();
  String sentence1 = ("The " + adjective + " " + subject + " " + verb + " at the " + object);
  System.out.println(sentence1);
   return subject;
    //return sentence1;
  }
public static String actionSubj(String subject){
  int randomNumber = (int)(Math.random()*2);
  String val = " ";
  //String subject = thesis();
  switch(randomNumber){
    case 0: val = ("This "+ subject);
    break;
    case 1: val = ("It");
      break;
  }
  return val;
}
  
  public static String adverb(){
    int randomNumber = (int) (Math.random() * 10);
    String val = " ";
    switch (randomNumber){
      case 0: val = "exaggeratingly";
        break;
      case 1: val = "excessively";
        break;
      case 2: val = "angrily";
        break;
      case 3: val = "particularly";
        break;
      case 4: val = "madly";
        break;
      case 5: val = "hurriedly";
        break;
      case 6: val = "excitedly";
        break;
      case 7:  val = "quickly";
        break;
      case 8: val = "stupidly";
        break;
      case 9: val = "exoticly";
        break;
    }
    return val;
  }
  
  public static String verb2(){
  int randomNumber = (int) (Math.random() * 10);
  int verbNum = randomNumber;
  String val = " ";
  switch(verbNum){
  case 0: val = "was";
          break;
   case 1: val = "spewed";
            break;
   case 2: val = "stared";
            break;
   case 3: val = "sighed";
            break;
   case 4: val = "smiled";
            break;
   case 5: val = "laughed";
            break;
   case 6: val = "cried";
            break;
   case 7: val = "giggled";
            break;
   case 8: val = "played";  
            break;
   case 9: val = "wrote";
            break;
}
  return val;
  
}
  //   This <fox> was <particularly> <mean> to <passing> <warplanes>.
  // <It> used <afterburners> to <spew> <plant matter> at the <fearsome> <cat>.
  public static String action(String subject){
    subject = actionSubj(subject);
    String verb = verb2();
    String adverb = adverb();
    String adj1 = adjective();
      String adj2 = adjective();
    String object = object();
      String sentence2 = (subject + " " + verb + " "+ adverb +" "+ adj1 + " to the " + adj2 +" " +object);
     // System.out.println(sentence2);   
    return sentence2;

  }
 
  public static String conclusion(String subject){
    String verb = verb2();
    String object = object();
    String sentence3 = ("That " + subject + " " + verb + " her " + object + ("!"));
    
    return sentence3;              
  }

  
  public static void paragraph(){
     String subject = thesis();
     int randomNumber = (int) (Math.random() * 10);
    String action;
    while(randomNumber >= 0){
  action = action(subject);
  System.out.println(action);
      randomNumber --;
    }
    System.out.println(conclusion(subject));
  }
    
public static void main(String[] args){
    boolean correct = false;
  
  while (correct == false){
  paragraph();
    Scanner scan = new Scanner( System.in );
     

  int answer = 0; 
  System.out.println("Would you like to print a new paragraph? Type 1 to rerun and anything other number for no.");
 // System.out.print(answer);
    while (!scan.hasNextInt()){
      scan.next();
    }
    answer = scan.nextInt(); 
    //System.out.print(correct);
    if (answer == 1) {
      correct = false;
    }
    else {
      correct = true;
    }
  }
}
} 