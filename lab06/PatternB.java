// Kristine Phan
// 10/11/18
// lab6 pattern b
// // Ask the user for an integer between 1 - 10. This will be the length of the pyramid (number of rows).
// print them in inverted pyramid formation from 1 to the integer they input 
// use nested loop

import java.util.Scanner;

public class PatternB{
  public static void main(String[] args){
    

Scanner scan = new Scanner (System.in);
    int numRows;
    int column;
    int input = 0;
    boolean correct = false;
    
    
    System.out.print("Input an integer: ");
    while (!correct){
     if (!scan.hasNextInt()) {
       System.out.println("Type in an integer.");
       scan.next();
     }
      else {
        input = scan.nextInt();
        if (input >= 1 && input <= 10){
          correct = true;
        }
        else {
          System.out.println("Type an integer in range.");
        }
      }
    }
    for (numRows = 0; numRows < input; numRows++){
      for (column = 1; (column <= (input - numRows)); column++){
        System.out.print(column+" ");
      }
      System.out.println();
    }

  }
}