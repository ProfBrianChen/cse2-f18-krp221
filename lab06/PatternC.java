
// Kristine Phan
// 10/11/18
// lab6 pattern c
// // Ask the user for an integer between 1 - 10. This will be the length of the pyramid (number of rows).
// print them in pyramid formation from 1 at the top and on the right side, ascending to the left of the rows
// the numbers should be descending pyramid formation from top to left bottom without spaces
// use nested loop

import java.util.Scanner;

public class PatternC{
  public static void main(String[] args){
    

Scanner scan = new Scanner (System.in);
    int numRows;
    int column;
    int input = 0;
    int number;
    boolean correct = false;
    
    
    System.out.print("Input an integer: ");
    while (!correct){
    
      if (!scan.hasNextInt()) {
       System.out.print("Type in an integer. ");
       scan.next();
      } 
      else {
        input = scan.nextInt(); 
        if (input >= 1 && input <= 10){
          correct = true;
        }
        else {
          System.out.print("Type an integer in range. ");
        }
      }
    }
    for (numRows = 1; numRows <= input; numRows++){
          for (column = 1; column <= (input-numRows) && column > 0; column++){   
                  System.out.print(" ");
                     }
          for (column = (input-numRows+1); column > (input-numRows) && column <= input; column++) {
                number = input-column+1;
                System.out.print(number--);
                
          }
                      
                
          System.out.println();
    }
        }
  }

