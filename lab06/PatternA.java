// Kristine Phan
// 10/11/18
// lab06
// Ask the user for an integer between 1 - 10. This will be the length of the pyramid (number of rows).
// print them in pyramid formation from 1 to the integer they input with spaces
// use nested loop

import java.util.Scanner;

public class PatternA{
  public static void main(String[] args){
    
    boolean correct = false;
    int input = 0;
    int numRows = 0;
    
    Scanner scan = new Scanner( System.in );
    
    System.out.println("Type an integer from 1-10:");
   // correct = scan.hasNextInt();
    while (!correct){
      if (!scan.hasNextInt()){
        scan.next();
        System.out.println("Type an integer.");
      }
      else {
      input = scan.nextInt();
        if (input < 1 || input > 10){
         System.out.print("Type an integer in range.");
       //  scan.next();
        }
       else{  
         correct = true;
       }
        }  
    }
    //System.out.print(input);
 for(numRows = 1; numRows <= input; ++numRows){
   for (int column = 1; column <= numRows; ++column){
     System.out.print(column + " ");
   }
  System.out.println();
 }
    
  }
}
  