
// Kristine Phan
// 10/11/18
// lab6 pattern d
// // Ask the user for an integer between 1 - 10. This will be the length of the pyramid (number of rows).
// print them in inverted pyramid formation from 1 at the bottom and on the right side, ascending to the top of the columns with spaces
// and the right of the rows
// use nested loop

import java.util.Scanner;

public class PatternD{
  public static void main(String[] args){
    

Scanner scan = new Scanner (System.in);
    int numRows;
    int column;
    int input = 0;
    int number = 0;
    boolean correct = false;
    
    
    System.out.print("Input an integer: ");
    while (!correct){
    
      if (!scan.hasNextInt()) {
       System.out.print("Type in an integer. ");
       scan.next();
      } 
      else {
        input = scan.nextInt(); 
        if (input >= 1 && input <= 10){
          correct = true;
        }
        else {
          System.out.print("Type an integer in range. ");
        }
      }
    }
    for (numRows = 1; numRows <= input; numRows++){
          for (column = (input-numRows+1); column <= (input-numRows+1)  && column >0; column--){   
                  System.out.print(column);
                  System.out.print(" ");
          }
                      
                
          System.out.println();
    }
        }
  }
