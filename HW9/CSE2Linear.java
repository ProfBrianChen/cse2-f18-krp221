// Kristine Phan
// 11/27/18
//  prompts the user to enter 15 ints for students’ final grades in CSE2
//Check that the user only enters ints
    //print an error message if the user enters anything other than an int.   
    //Print a different error message for an int that is out of the range from 0-100
    //third error message if the int is not greater than or equal to the last int.   
//Print the final input array.
//prompt the user to enter a grade to be searched for. 
    //Use binary search to find the entered grade. 
    //Indicate if the grade was found or not, and print out the number of iterations used. 
// scramble the sorted array randomly
  //print out the scrambled array. 
  //Prompt the user again to enter an int to be searched for
  //use linear search to find the grade
 // ***Indicate if the grade was found or not, and how many iterations it took. 

//Write separate methods for linear search, binary search and random scrambling and print method to track the progress of your array. Import and use java.util.Random for the random scrambling method. 
import java.util.Scanner; // imoprt scanner 
import java.util.Random; // import java.util.Random for random scrambling
public class CSE2Linear{
    
  //prompt the user to enter a grade to be searched for. 
    //Use binary search to find the entered grade. 
    //Indicate if the grade was found or not, and print out the number of iterations used. 
  
  public static void linear(int[] array, int search){ // linear search for grade 
    for (int i = 0; i<array.length; i++){
      if (array[i] == search){ // 
        System.out.println(search+" was found in the list with "+i+1+" iterations"); //i increments, it is the number of iterations taken to find the searched grade
        return; //end
      }
      if (i == array.length-1 && array[i] != search){ // if we reach the last iteration and it does not have the searched grade, return the number of i
      System.out.println(search+" was not found in the list with "+(i+1)+" iterations"); //i increments, it is the number of iterations taken to find the searched grade
      return; // end 
      }
    }
  }
 
  
  public static void binary(int[] array, int search){
    int count = 1; // initialize iterations
    int first = 0; // initialize first index
    int last = array.length-1; //initialize last index
    while (first <= last){ // keep running until u run out of divides of divide and conquer
      int middle = (first + last)/2; // initialize middle number
      if (search > array[middle]){ // go through second half if search is greater than the middle number
        first = middle + 1; // make first index the second half's first index
      }
      else if (search < array[middle]){// go through first half if search is less than middle number
        last = middle - 1; //make last number of search the last of the first half
}
      else {
        System.out.println(search+" was found in the list with " +count+" iteration(s)."); // if it is the middle number of the divide, it is found and tells how many iterations it otok
        return; //end once found
      }
      count++; //increase count each time it searches
    }
    System.out.println(search+" was not found with "+ count+" iterations."); // if it's not found
    } 

  
  public static void print(int[] arrays){ //print array method
      for (int i = 0; i<arrays.length; i++){
        System.out.print(arrays[i]+" "); // print every array elem
      }
              System.out.println(); //new line
    }

  
  public static int[] scrambler(int[] array){ //scrambler method
  int temp; 
  Random randGen = new Random();  // New random number generator
    int swap = 0;
  for(int i = 0; i < 100; i++){ //do swap 100 times
   temp = array[0]; 
   swap = randGen.nextInt((array.length));// generate random index
  array[0] = array[swap]; // swap index 0 with content of random index
  array[swap] = temp;  
  }

  return array; //return array of shuffled deck as new value of cards
 }

  
public static void main(String[] args){
  Scanner scan = new Scanner(System.in);
  
  System.out.print("Type in 15 ascending integers for the students' final grades in CSE2 and press enter between each integer."); // ask user for 15 numbers in ascending order
  int list[] = new int[15]; // initialize array of 15 elem
  boolean correct = false; // initialize boolean
  int input; // initialize first input of grade user wants to find

  int input1; // initialize 2nd input of grade user wants to find
  for (int i = 0; i<list.length; i++){ // continue loop until user puts in all 15 values
    correct = false; //make condition false again to continue loop for all 15 values
    while(!correct){  
    if (!scan.hasNextInt()) {
       System.out.print("Type in an integer."); // error message if not an integer and go through while again
       scan.next();
      } 
      else {
        list[i] = scan.nextInt();  // if an integer, accept it
        if (list[i] < 1 || list[i] > 100){ // if not in range, print error message and go through while loop again
          System.out.print("Type an integer in range. ");
        }
        else {
          if (i > 0 && list[i] < list[i-1]){ // if not ascending, print error msg and go thru while loop again
            System.out.print("Type an integer greater than or equal to the last integer.");
          }
          else {
            correct = true; // ONLY if it is ascending AND an int AND in range, accept the value of the int and make it array index's final val
          }
        }
      }
    }
  }
   print(list); // print array 
  System.out.println("Name a grade to be found.");//Ask user for a grade to b found
while(!scan.hasNextInt()) {
       System.out.print("Type in an integer."); // error msg if not an int
       scan.next();
      } 
input = scan.nextInt(); // accept user input
  binary(list,input); // go through binary method
  print(scrambler(list));  // go through scrambler method and print the array 

 System.out.println("Name another grade to be found."); // ask user for another grade
while(!scan.hasNextInt()) {
       System.out.print("Type in an integer."); // error msg
       scan.next();
      } 
input1 = scan.nextInt(); //accept user input
 
linear(list,input1); // take input and go through linear method 

  
}
}
