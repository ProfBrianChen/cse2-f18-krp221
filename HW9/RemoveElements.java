// Kristine Phan
// hw 9
// 11/27/18
// make randomInput() method: generates an array of 10 random integers between 0 to 9.  Implement randomInput so that it fills the array with random integers and returns the filled array. 
// make method delete(list,pos) takes, as input, an integer array called list and an integer called pos
//  creates a new array that has one member fewer than list, and be composed of all of the same members except the member in the position pos.  


import java.util.Scanner; // import scanner
import java.util.Random; // import random gen
public class RemoveElements{
  
public static int[] randomInput(){ //method to get array of 10 random number
    Random randGen = new Random();
    int list[] = new int[10]; // initialize array of 10 integers

for (int i = 0; i < list.length; i++){
  list[i] =  randGen.nextInt(10); //make array of numbers that range from 0-9
}
    return list; //return array
  }
  
  
  public static int[] delete(int[] list, int pos){ // make array that deletes value at posn that user inputs
    if (pos < 0 || pos >= list.length){ 
      System.out.println("This index is not valid."); //if they input an index out of bounds, tell them it is 
      return list; // return original array if the index doesn't exist
    }
    else{
      System.out.println("Index "+pos+" is removed."); //if index exists tell user that index at that posn is removed
    }
    int array[] = new int[list.length-1]; // create a new array that has one less elements bc it removed one
    for (int i = 0; i < list.length; i++){ //set each array elem
      if (i < pos){
        array[i] = list[i]; // every num before the posn deleted will b the same index
      }
      else if (i > pos) {
        array[i-1] = list[i]; // every num after the posn deleted will be moved to the index before it in the new array
      }
    }
    return array; // return array
  }
  
  public static int[] remove(int[] list, int target){
    int count = 0;  // initialie counter
    int counter = 0; // initialize second counter
        for(int i = 0; i < 10; i++){
      if (list[i] == target){
        count++; // for every number that is the same as the target in the array, count how many
      }
		}
    if (count == 0){ 
      System.out.println("Element not found. "); // element not found if going through count of target does not incr
      return list; // return original array
    }
    else {
      System.out.println("Element "+target+" has been found."); // if there is target in them, notify user 
    }
    int[] array = new int[list.length - count]; // make a new array without the number the user entered
for (int i = 0; i < list.length; i++){ // go through entire array of original array
  if (list[i] != target){ 
    array[counter] = list[i]; // set each elem of array to og array's content that's not the target
    counter++; // increase counter so the new array increases after it has a value that is not the target
  }
}
    return array;
	}
  
  
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.println("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
