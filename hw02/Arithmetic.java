// Kristine  Phan
// CSE 2 - 311
// Arithmetic Calculations -- compute the cost of the items you bought, including the PA sales tax of 6%

// 9/6/18
public class Arithmetic{
  public static void main(String args[]){

// input variables
int numPants = 3; //Number of pairs of pants
double pantsPrice = 34.98; //Cost per pair of pants
int numShirts = 2; //Number of sweatshirts
double shirtPrice = 24.99; //Cost per shirt
int numBelts = 1; // Number of belts
double beltCost = 33.99; //cost per belt
double paSalesTax = 0.06; //the tax rate
int conversion1 = 100; // converting decimals with more than 2 digits to an integer 
double conversion2 = 100.0; //converting integers back to decimals with 2 decimal places
    
// run calculations. 
double totalCostofPants = (numPants * pantsPrice); // total cost of pants before tax
double pantsTax = (numPants * pantsPrice * paSalesTax) * conversion1; // accurate sales tax for pants multiplied by 100 to later get 2 decimal places
  int intPantsTax = (int) pantsTax; // conversion of pants tax to integer to later get to 2 decimal places
  double estimPantsTax = intPantsTax / conversion2; // conversion of pants tax back to decimal with only 2 decimal places
    
double totalCostofShirts = (numShirts * shirtPrice); //total cost of shirts before tax
double shirtsTax = (numShirts * shirtPrice * paSalesTax) * conversion1; // accurate sales tax for all shirts multiplied by 100 to later get 2 decimal places
  int intShirtsTax = (int) shirtsTax; // conversion of shirts tax to integer to later get to 2 decimal places
  double estimShirtsTax = intShirtsTax / conversion2; //conversion of shirts tax back to decimal with only 2 decimal places

double totalCostofBelts =  (numBelts * beltCost); //total cost of belts before tax
double beltsTax = (numBelts * beltCost * paSalesTax) * conversion1; // accurate sales tax for belt multiplied by 100 to later get 2 decimal places
  int intBeltsTax = (int) beltsTax; //conversion of belt tax to integer to later get to decimal places
  double estimBeltsTax = intBeltsTax / conversion2; //conversion of belts tax back to decimal with only 2 decimal places

double totalSalesTax = (pantsTax + shirtsTax + beltsTax); //accurate total sales tax (multiplied by 100) to later get 2 decimal places
    int intTotalSalesTax = (int) totalSalesTax; //conversion of total sales tax to integer to later get to decimal places
    double estimTotalSalesTax = intTotalSalesTax / conversion2; //conversion of total sales tax back to decimal with only 2 decimal places
double totalCost = (totalCostofPants + totalCostofShirts + totalCostofBelts); //total cost before tax 
double totalPaid  = (totalCost + estimTotalSalesTax); //total paid for transaction including sales tax 


// print calculations
 System.out.println("The total cost of pants before tax is $"+totalCostofPants+ " and the sales tax paid for all the pants is $"+estimPantsTax+"."); // Print total cost of pants before tax and sales tax for pants
 System.out.println("The total cost of shirts before tax is $"+totalCostofShirts+ " and the sales tax paid for all the shirts is $"+estimShirtsTax+"."); // Print total cost of shirts before tax and sales tax for shirts
 System.out.println("The total cost of belts before tax is $"+totalCostofBelts+ " and the sales tax paid for all the belts is $"+estimBeltsTax+"."); // Print total cost of belt before tax and sales tax for belt
 System.out.println("The total cost of the purchases before tax is $"+totalCost+"."); // Print total cost of purchases before tax
 System.out.println("The total sales tax is $"+estimTotalSalesTax+"."); // Print total sales tax for all of the items combined
 System.out.println("The total cost of the purchases including sales tax is $"+totalPaid+"."); // Print total paid for transaction including sales tax

  }
}
