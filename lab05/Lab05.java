// Kristine Phan
// CSE 2 311
// 10/4/18
//  ask for the course number, department name, the number of times it meets in a week, 
// the time the class starts, the instructor name, and the number of students
// loop again until they say it 
import java.util.Scanner;

public class Lab05{
public static void main(String[] args){
Scanner scan = new Scanner( System.in );
int counter = 0;
int crn = 0;
String name = "";
double time = 0;
int freq = 0;
String prof = "";
int size = 0;
boolean correct;

while (counter < 6 & counter >= 0) {

  if (counter == 0){
  System.out.print("What is your course number?"); // ask student for course number
  correct = scan.hasNextInt(); // accept integer input
    if (correct) {
      crn = scan.nextInt();
      counter = 1; // move onto next question
    }
    else {
      scan.next();
      counter = 0;
    }
    }
  else if (counter == 1) {
  System.out.print("What is the department name of this class?");
//   correct = scan.hasNext();
//     if (correct) {
    name = scan.next(); // accept string
    counter = 2; // move to next question
//     }
//       else {
//         scan.next();
//         counter = 1;
//       }
  }
   else if (counter == 2){
  System.out.print("How many times do you meet a week?"); // ask student for frequency of class
    correct = scan.hasNextInt();
     if (correct) {
    freq = scan.nextInt(); // accept integer input
    counter = 3; // move onto next question
     }
     else {
      scan.next();
     counter = 2;
     }
    } 
else if (counter == 3){
 System.out.print("What time do you meet in form of an integer?"); // ask student for time they meet 
  correct = scan.hasNextInt();
  if (correct) {
  time = scan.nextInt(); // accept int input
  counter = 4; // move onto next question
  }
  else {
    scan.next();
    counter = 3;
  }
    }
else if (counter == 4){
 System.out.print("What is your professor's name?"); // ask student for prof name
 correct = scan.hasNext();
  if (correct) {
    prof = scan.next(); // accept string input
    counter = 5; // move onto next question
  }
    else {
      scan.next();
      counter = 4;
    }
    } 
else {
  System.out.print("How many students do you have in your class?"); // ask student for number of students in class
   correct = scan.hasNextInt();
  if (correct) {
  size = scan.nextInt(); // accept integer input
  counter = 6; // end
  }
    else {
    counter = 5; // go back to  question 
      scan.next();
    } 
    }
    
}    
 }
}


  