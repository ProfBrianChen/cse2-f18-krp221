// Kristine Phan
// CSE 002-311 Cyclometer
// 9/6/18
// Bicycle cyclometer records time elapsed in seconds, number of rotations during elapsed time, 

public class Cyclometer{
  
  public static void main(String args[]){
    /// set initials
    double secsTrip1 = 480;  // declare and assign seconds elapsed per trip 1
    double secsTrip2 = 3220; // declare and assign seconds elapsed per trip 2
    int rotations1 = 1561; // declare and assign number of rotations per trip 1
    int rotations2 = 9037;  // declare and assign number of rotations per trip 2
    double pi = 3.14159; // declare pi to convert rotations to rads
    double wheelDiameter = 27.0; // declare and assign wheel diameter
    int feetPerMile = 5280; // convert feet to miles
    int inchesPerFoot = 12; // convert inches to feet
    int secsPerMin = 60;       // declare and assign number of seconds per minute to convert seconds measured to minutes
   
    // Run calculations, store data 
    
    double distTrip1 = rotations1 * wheelDiameter * pi / inchesPerFoot / feetPerMile; // Calculate total distance of trip 1
    double distTrip2 = rotations2 * wheelDiameter * pi / inchesPerFoot / feetPerMile; // Calculate total distance of trip 2
    double totalDistance = distTrip1 + distTrip2; // Add distance of trip 1 and trip 2 for total distance
    
    // Print output data 
    
    System.out.println("Trip 1 took "+ (secsTrip1/secsPerMin)+" minutes and had "+rotations1+" counts.");   
      // Print number of minutes and number of counts during trip 1
    System.out.println("Trip 2 took "+ (secsTrip2/secsPerMin)+" minutes and had " +rotations2+ " counts.");
      // Print number of minutes and number of counts during trip 2 
    System.out.println("Trip 1 was "+distTrip1+" miles");
      // Print out distance of trip 1 in miles
	  System.out.println("Trip 2 was "+distTrip2+" miles");
      // Print out distance of trip 2 in miles
	  System.out.println("The total distance was "+totalDistance+" miles");
      // Print out total distance of trip 1 and 2 

  }
}