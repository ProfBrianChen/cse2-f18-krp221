// Kristine Phan
// CSE2 311
// hide the secret message X and print out X through spaces buried in a handful of stars 
// side length of grid should be from input of user
// "x" comes from spaces when the nth column is the same n as the row and at the column that is equal to the entire grid (input) minus the nth row
// 10/20/18

import java.util.Scanner; // import scanner

public class EncryptedX{
  public static void main(String[] args){
    
   Scanner scan = new Scanner( System.in );
    int input = 0; //initialize input
    int row; // initialize row 
    int column; // initialize column 
    boolean correct = true; // initialize boolean tru
    char star = '*'; // intialize character * that makes outside of X
    
     
    System.out.print("Enter an integer from 0 -100: "); // Ask user for their input
    
    while (correct){ // run loop of asking user for integer (until it is the right format and range)
      if (!scan.hasNextInt()){ 
        System.out.println("Enter an integer. "); // tell them to enter an integer if it is not an integer
         scan.next(); // scan again
       }
                          
      else {
        input = scan.nextInt(); // if it's an integer, register the value
        if (input >= 0 && input <= 100) { // if it is in range (from 0 -100), get out of the loop of asking the user for an input and keep the input as is
          correct = false;
        }
          else {
            System.out.println("Enter an integer in range."); // tell the user is out of range and make them run the code again since we cannot use the stored input value
          }
      }  
      }
      
    for (row = 1; row <= input; row++){ // run loop until amount of rows that user has inputted is reached
      for (column = 1; column <= input; column++){ // run columns for every row (same number as rows bc it's a square)
        if (column == (input+1 - row)){ // put space for X when column is same as row from right side
          System.out.print(" ");
        }
        else if (column == row){ // put space for X when column is same as row from left side
          System.out.print(" ");
        }
        else {
        System.out.print(star); // print * for every other place there isn't a space for X
        }
      }
      System.out.println(); // go to next line at end of every row
    }
    
    
    
  }
}