// Kristine Phan
// CSE 2 311
// 10/8/18
// Your program should ask the user how many times it should generate hands. 
// For each hand, your program should generate five random numbers from 1 to 52, 
// where these 52 cards represent the four sets of 13 cards associated with each suit.  Specifically, cards 1-13 represent diamonds; 
// 14-26 represent clubs; then hearts; then spades.  In all suits, card identities ascend in step with the card number
// Your program should print the number of loops as well as the probability associated with each of the four hands. 
// Please print the probabilities as decimals with three decimal places represented.
 
import java.util.Scanner;

public class Hw05{
  public static void main(String[] args){
    Scanner scan = new Scanner( System.in );
    boolean correct = true; // initialize boolean
    int hands = 1; // initialize hands 
    int count = 1; // initialize count 
    // initialize face value for all 5 cards
    int faceValue1 = 1;  
    int faceValue2 = 1;
    int faceValue3 = 1;
    int faceValue4 = 1;
    int faceValue5 = 1;
    // initialize types of things you can get with the face cards
    int fourOfAKind = 0;
    int threeOfAKind = 0;
    int twoPair = 0;
    int onePair = 0;
    // initialize probability of getting 4 of a kind, 3 of a kind, 2 pair, and 1 pairs
    double probFour = 0;
    double probThree = 0;
    double probTwo = 0;
    double probOne = 0;
    
    
    System.out.println("Input how many times we should generate hands: "); // ask user for how many hands should be generated
    
    while (correct){ // run loop until user enters an integer
      if (!scan.hasNextInt()) {
      scan.next();
      System.out.print("Type in an integer.");
    }
    else {
      hands = scan.nextInt(); 
      correct = false;
    }
    }
    
    while (count >= 1 && count <= hands) { // run loop until it reaches number of hands user inputted 
    int randomNumber1 = (int) (Math.random() * 52)+1; // Create range of random numbers from 1 to 52 for specific card 1
    int randomNumber2 = (int) (Math.random() * 52)+1; // Create range of random numbers from 1 to 52 for specific card 2
      while (randomNumber2 == randomNumber1){ // make sure they're diff
        randomNumber2 = (int) (Math.random() * 52)+1;
      }
    int randomNumber3 = (int) (Math.random() * 52)+1; // Create range of random numbers from 1 to 52 for specific card 3
      while (randomNumber3 == randomNumber2 || randomNumber2 == randomNumber1){ // make sure they're diff
        randomNumber3 = (int) (Math.random() * 52)+1;
      }
    int randomNumber4 = (int) (Math.random() * 52)+1; // Create range of random numbers from 1 to 52 for specific card 4
      while (randomNumber4 == randomNumber2 || randomNumber4 == randomNumber1 || randomNumber4 == randomNumber3){ // make sure they're diff
        randomNumber4 = (int) (Math.random() * 52)+1;
      }
    int randomNumber5 = (int) (Math.random() * 52)+1; // Create range of random numbers from 1 to 52 for specific card 5
    while (randomNumber5 == randomNumber2 || randomNumber5 == randomNumber1 || randomNumber5 == randomNumber3 || randomNumber5 == randomNumber4){ // make sure they're diff
        randomNumber5 = (int) (Math.random() * 52)+1;
      }
      // find face values of all 5 cards
   faceValue1 = randomNumber1 % 13; 
   faceValue2 = randomNumber2 % 13;
   faceValue3 = randomNumber3 % 13;
   faceValue4 = randomNumber4 % 13;
   faceValue5 = randomNumber5 % 13;
   
      // if 4 cards are the same facevalue, add "1" to  four of a kind each time the loop runs
      if (faceValue1 == faceValue2 && faceValue2 == faceValue3 && faceValue3 == faceValue4){
        fourOfAKind++; 
      }
        else if  (faceValue1 == faceValue2 && faceValue2 == faceValue3 && faceValue3 == faceValue5){
          fourOfAKind++;
        }
        else if (faceValue2 == faceValue3 && faceValue3 == faceValue4 && faceValue4 == faceValue5){
          fourOfAKind++;
        }
          else if (faceValue1 == faceValue3 && faceValue3 == faceValue4 && faceValue4 == faceValue5){
            fourOfAKind++;
          }
        else if (faceValue1 == faceValue2 && faceValue2 == faceValue4 && faceValue4 == faceValue5) {
          fourOfAKind++;
        }
      // if 3 cards are the same facevalue, add "1" to  3 of a kind each time the loop runs  
      else if (faceValue1 == faceValue2 && faceValue2 == faceValue3){
          threeOfAKind++;
      }
        else if (faceValue1 == faceValue2 && faceValue2 == faceValue4){
          threeOfAKind++;
        }
      else if (faceValue1 == faceValue2 && faceValue2 == faceValue5) {
          threeOfAKind++;
        }
      else if (faceValue1 == faceValue3 && faceValue3 == faceValue4) {
          threeOfAKind++;
        }
        else if (faceValue1 == faceValue3 && faceValue3 == faceValue5){
          threeOfAKind++;
        }
        else if (faceValue1 == faceValue4 && faceValue4 == faceValue5){
          threeOfAKind++;
        }
        else if (faceValue2 == faceValue3 && faceValue3 == faceValue4) {
          threeOfAKind++;
        }
        else if (faceValue1 == faceValue3 && faceValue3 == faceValue5) {
          threeOfAKind++;
        }
        else if (faceValue2 == faceValue4 && faceValue4 == faceValue5) {
          threeOfAKind++;
        }
        else if (faceValue3 == faceValue4 && faceValue4 == faceValue5) {
          threeOfAKind++;
        }
      // if 4 cards are the same 2 FACE values, add "1" to  two pair each time the loop runs
        else if (faceValue1 == faceValue2 && faceValue3 == faceValue4) {
          twoPair++;
        }
        else if (faceValue1 == faceValue2 && faceValue3 == faceValue5) {
          twoPair++;
        }
        else if (faceValue1 == faceValue2 && faceValue4 == faceValue5) {
          twoPair++;
        }   
        else if (faceValue1 == faceValue3 && faceValue2 == faceValue4) {
          twoPair++;
        }
        else if (faceValue1 == faceValue3 && faceValue2 == faceValue5) {
          twoPair++;
        }
        else if (faceValue1 == faceValue3 && faceValue4 == faceValue5) {
          twoPair++;
        }
        else if (faceValue1 == faceValue4 && faceValue3 == faceValue2) {
          twoPair++;
        }
        else if (faceValue1 == faceValue4 && faceValue2 == faceValue5) {
          twoPair++;
        }
        else if (faceValue1 == faceValue4 && faceValue3 == faceValue5) {
          twoPair++;
        }
        else if (faceValue2 == faceValue3 && faceValue1 == faceValue4) {
          twoPair++;
        }
        else if (faceValue3 == faceValue2 && faceValue1 == faceValue5) {
          twoPair++;
        }
        else if (faceValue3 == faceValue2 && faceValue5 == faceValue4) {
          twoPair++;
        }
        else if (faceValue4 == faceValue2 && faceValue1 == faceValue4) {
          twoPair++;
        }
        else if (faceValue4 == faceValue2 && faceValue3 == faceValue5) {
          twoPair++;
        }
        else if (faceValue1 == faceValue5 && faceValue3 == faceValue2) {
          twoPair++;
        }
        else if (faceValue1 == faceValue5 && faceValue3 == faceValue4) {
          twoPair++;
        }
        else if (faceValue5 == faceValue2 && faceValue3 == faceValue4) {
          twoPair++;
        }
      // if 2 cards are the same FACE values, add "1" to  1 pair each time the loop runs
        else if (faceValue1 == faceValue2 && faceValue3 != faceValue4 && faceValue4 != faceValue5){
          onePair++;
        }
        else if (faceValue1 == faceValue3 && faceValue2 != faceValue4 && faceValue4 != faceValue5){
          onePair++;
        }
        else if (faceValue1 == faceValue4 && faceValue3 != faceValue2 && faceValue2 != faceValue5){
          onePair++;
        }
        else if (faceValue1 == faceValue5 && faceValue2 != faceValue3 && faceValue4 != faceValue3){
          onePair++;
        }
         else if (faceValue2 == faceValue3 && faceValue1 != faceValue4 && faceValue4 != faceValue3){
          onePair++;
        }       
        else if (faceValue2 == faceValue4 && faceValue1 != faceValue3 && faceValue3 != faceValue5){
          onePair++;
        }
        else if (faceValue2 == faceValue5 && faceValue1 != faceValue3 && faceValue3 != faceValue4){
          onePair++;
        }
        else if (faceValue3 == faceValue4 && faceValue2 != faceValue1 && faceValue1 != faceValue5){
          onePair++;
        }
        else if (faceValue3 == faceValue5 && faceValue2 != faceValue4 && faceValue4 != faceValue1){
          onePair++;
        }
        else if (faceValue4 == faceValue5 && faceValue2 != faceValue3 && faceValue3 != faceValue1){
          onePair++;
        }
      
      count++; // increase count every time loop runs
      }
    // calculate probabilities for total 4 of a kind, 3 of a kind, 2 pair, & 1 pairs to total hands 
    probFour = (double)fourOfAKind/(double)hands;
     probThree = (double)threeOfAKind/(double)hands;
     probTwo = (double) twoPair/(double)hands;
     probOne = (double) onePair/(double)hands;
    // print number of loops and all probabilities
     System.out.printf("Number of loops: " + hands+"\n");
      System.out.printf("The probability of Four-of-a-kind: %4.3f\n", probFour);
      System.out.printf("The probability of a Three-of-a-kind: %4.3f\n", probThree);
      System.out.printf("The probability of a Two Pair: %4.3f\n", probTwo);
      System.out.printf("The probability of a One Pair: %4.3f\n", probOne);
      }
      }