// Kristine Phan
// CSE 2 311 
// 9/25/18
// Ask the user if they would randomly cast dice or if they would like to state the two dice they want to evaluate
// If they want randomly cast dice, generate two random numbers from 1-6, inclusive, representing the outcome of the two dice cast in the game.
// If they want user provided dice, use the Scanner twice to ask the user for two integers from 1-6, inclusive.  Assume the user always provides an integer, but check if the integer is within the range.
// Determine the slang terminology of the outcome of the roll
// Print out the slang terminology
// Exit
// Use only if!!!!

import java.util.Scanner; // import Scanner to use it
// Add class & methpd
public class CrapsIf{
  public static void main(String[] args){
    
    String name; // Declare variable of name of outcome of roll
    int number1; // Declare and assign variable for first number of dice, either randomized or user's input
    int number2; //Declare and assign variable for second number of dice, either randomized or user's input
    int randomDice1 = (int) (Math.random() * 6)+1; // Declare/assign variable of 1st random number generated
    int randomDice2 = (int) (Math.random() * 6)+1; // Declare/assign variable of 2nd random number generated
    
    Scanner myScanner = new Scanner( System.in ); //Declare instance of scanner

    //Get user to input if they'd rather randomly cast dice or if they'd rather state the two dice they'd want to evaluate
    System.out.print("Type 1 if you would like to randomly cast dice. Type 2 if you'd like to state two dice you want to evaluate: "); 
    // Declare/assign variable of choice that they make -- either randomly casting dice or inputting two numbers
    int choice = myScanner.nextInt();
    
    // Randomly generate two dice
    if (choice == 1){ 
      // Assign variables of first number of dice & second to a randomly generated one
      number1 = randomDice1;
      number2 = randomDice2;
      int score = number1 + number2; // Declare/assign variable of score by adding two numbers on dice
      // Assign names to combinations of numbers
      if (number1 == 1 && number2 == 1) {
        name = "Snake Eyes"; // Assign slang term of snake eyes if both dice are 1
      }
      else if (number1 == 1 && number2 == 2 || number1 == 2 && number2 == 1) {
        name = "Ace Deuce"; // Assign slang term of ace deuce if one of the dice is a 1 and the other is a 2 
      }
      else if (number1 == 1 && number2 == 3 || number1 == 3 && number2 == 1) {
        name = "Easy Four"; // assign slang term of easy four if one dice is 1 & other 3
      }
      else if (number1 == 1 && number2 == 4 || number1 == 4 && number2 == 1){
        name = "Fever Five";// assign slang term of fever 5 if one dice is 1 & other 4
      }
      else if (number1 == 1 && number2 == 5 || number1 == 5 && number2 == 1){
        name = "Easy Six"; // assign slang term of easy six if one dice is 1 & other 5
      }
      else if (number1 == 1 && number2 == 6 || number1 == 6 && number2 == 1){
        name = "Seven Out"; // assign slang term of seven out if one dice is 1 & other 6
      }
      else if (number1 == 2 && number2 == 2){
        name = "Hard Four"; // assign slang term of hard 4 if both dice are 2s
      }
      else if (number1 == 2 && number2 == 3 || number1 == 3 && number2 == 2) {
       name = "Fever Five"; // assign slang term of fever 5 if one dice is 2 & other 3
      }
      else if (number1 == 2 && number2 == 4 || number1 == 4 && number2 == 2) {
       name = "Easy Six"; // assign slang term of easy six if one dice is 2 & other 4
      }
      else if (number1 == 2 && number2 == 5 || number1 == 5 && number2 == 2) {
       name = "Seven Out"; // assign slang term of seven out if one dice is 2 & other 5
      }
      else if (number1 == 2 && number2 == 6 || number1 == 6 && number2 == 2) {
       name = "Easy Eight"; // assign slang term of easy 8 if one dice is 2 & other 6
      }
      else if (number1 == 3 && number2 == 3) {
       name = "Hard Six"; // assign slang term of hard six if both dice is 6
      }
      else if (number1 == 3 && number2 == 4 || number1 == 4 && number2 == 3) {
       name = "Seven Out"; // assign slang term of seven out if one dice is 4 & other 3
      } 
      else if (number1 == 3 && number2 == 5 || number1 == 5 && number2 == 3) {
       name = "Easy Eight"; // assign slang term of easy eight if one dice is 5 & other 3
      }
      else if (number1 + number2 == 9) {
       name = "Nine"; // assign slang term of easy four if dice add up to 9
      } 
      else if (number1 == 4 && number2 == 4) {
       name = "Hard Eight"; // assign slang term of hard eight if both are 4s
      }
      else if (number1 == 4 && number2 == 6 || number1 == 6 && number2 == 4){
        name = "Easy Ten"; // assign slang term of easy ten if one dice is 6 & other 4
      }
      else if (number1 == 5 && number2 == 5){
        name = "Hard Ten"; // assign slang term of hard 10 if both dice are 5s
      }
      else if (number1 == 5 && number2 == 6 || number1 == 6 && number2 == 5){
        name = "Yo-Leven"; // assign slang term of yo leven if one dice is 5 & other 6
      }
      else {
        name = "Boxcars"; // assign slang term of box cars if both dice are 6s
      }
           // Print what they rolled, their score, and the slang term
            System.out.println("You rolled a "+number1+" and a "+ number2+". Your score is "+score+" and named "+name ); 

    }
    // Ask user for inputs of two dice
    else if (choice == 2){
      System.out.print("Type an integer from 1-6 for the first dice: "); // Ask user for number from 1-6
      int integer1 = myScanner.nextInt(); // accept user's input 
      boolean truth = true; // check if integer is in range
      
      if ( integer1 <= 6 && integer1 >= 1){ // chevk if integer is in range
        if( truth ){ 
		    System.out.print("Type an integer from 1-6 for the second dice: "); // Ask for second number if 1st number is in range
        int integer2 = myScanner.nextInt(); // Accept user's input
       
        if (integer2 <= 6 && integer2 >= 1){ 
          if (truth) { // go through with calculation of name if number is in range
            number1 = integer1; // Assign variable of number 1 to what user input
            number2 = integer2; // Assign variable of number2 to what user input
            // Assign names to combination of numbers on dice
            if (number1 == 1 && number2 == 1) {
              name = "Snake Eyes"; // Assign slang term of snake eyes if both dice are 1
            }
            else if (number1 == 1 && number2 == 2 || number1 == 2 && number2 == 1) {
              name = "Ace Deuce"; // Assign slang term of ace deuce if one of the dice is a 1 and the other is a 2 
            }
            else if (number1 == 1 && number2 == 3 || number1 == 3 && number2 == 1) {
              name = "Easy Four"; // assign slang term of easy four if one dice is 1 & other 3
            }
            else if (number1 == 1 && number2 == 4 || number1 == 4 && number2 == 1){
              name = "Fever Five";// assign slang term of fever 5 if one dice is 1 & other 4
            }
            else if (number1 == 1 && number2 == 5 || number1 == 5 && number2 == 1){
              name = "Easy Six"; // assign slang term of easy six if one dice is 1 & other 5
            }
            else if (number1 == 1 && number2 == 6 || number1 == 6 && number2 == 1){
              name = "Seven Out"; // assign slang term of seven out if one dice is 1 & other 6
            }
            else if (number1 == 2 && number2 == 2){
              name = "Hard Four"; // assign slang term of hard 4 if both dice are 2s
            }
            else if (number1 == 2 && number2 == 3 || number1 == 3 && number2 == 2) {
             name = "Fever Five"; // assign slang term of fever 5 if one dice is 2 & other 3
            }
            else if (number1 == 2 && number2 == 4 || number1 == 4 && number2 == 2) {
             name = "Easy Six"; // assign slang term of easy six if one dice is 2 & other 4
            }
            else if (number1 == 2 && number2 == 5 || number1 == 5 && number2 == 2) {
             name = "Seven Out"; // assign slang term of seven out if one dice is 2 & other 5
            }
            else if (number1 == 2 && number2 == 6 || number1 == 6 && number2 == 2) {
             name = "Easy Eight"; // assign slang term of easy 8 if one dice is 2 & other 6
            }
            else if (number1 == 3 && number2 == 3) {
             name = "Hard Six"; // assign slang term of hard six if both dice is 6
            }
            else if (number1 == 3 && number2 == 4 || number1 == 4 && number2 == 3) {
             name = "Seven Out"; // assign slang term of seven out if one dice is 4 & other 3
            } 
            else if (number1 == 3 && number2 == 5 || number1 == 5 && number2 == 3) {
             name = "Easy Eight"; // assign slang term of easy eight if one dice is 5 & other 3
            }
            else if (number1 + number2 == 9) {
             name = "Nine"; // assign slang term of easy four if dice add up to 9
            } 
            else if (number1 == 4 && number2 == 4) {
             name = "Hard Eight"; // assign slang term of hard eight if both are 4s
            }
            else if (number1 == 4 && number2 == 6 || number1 == 6 && number2 == 4){
              name = "Easy Ten"; // assign slang term of easy ten if one dice is 6 & other 4
            }
            else if (number1 == 5 && number2 == 5){
              name = "Hard Ten"; // assign slang term of hard 10 if both dice are 5s
            }
            else if (number1 == 5 && number2 == 6 || number1 == 6 && number2 == 5){
              name = "Yo-Leven"; // assign slang term of yo leven if one dice is 5 & other 6
            }
            else {
              name = "Boxcars"; // assign slang term of box cars if both dice are 6s
      }
            int score = number1 + number2; // Declare/assign variable of score by adding two numbers on dice
            // Print what they rolled, their score, and the slang term
            System.out.println("You rolled a "+number1+" and a "+ number2+". Your score is "+score+" and named "+name ); 

    }
  }
          else {
            System.out.println("Your input is out of range. Type an integer in the range of 1 to 6."); // tell them to print 2nd term in range
          }
      }
      }
        else {
          System.out.println("Your input is out of range. Type an integer in the range of 1 to 6."); // Tell them to print 1st term in range
        }
    }
    else {
      System.out.println("Your input is out of range. Type an integer in the range of 1 to 2.");
    }
  }
}