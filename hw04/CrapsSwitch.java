// Kristine Phan
// CSE 2 311
// 9/25/18
// Ask the user if they would randomly cast dice or if they would like to state the two dice they want to evaluate
// If they want randomly cast dice, generate two random numbers from 1-6, inclusive, representing the outcome of the two dice cast in the game.
// If they want user provided dice, use the Scanner twice to ask the user for two integers from 1-6, inclusive.  Assume the user always provides an integer, but check if the integer is within the range.
// Determine the slang terminology of the outcome of the roll
// Print out the slang terminology
// Exit
// Use only switch!!!!

import java.util.Scanner; //import Scanner

public class CrapsSwitch{
  public static void main (String[] args){
    String name = ""; // Declare variable of name of outcome of roll
    int number1; // Declare and assign variable for first number of dice, either randomized or user's input
    int number2; //Declare and assign variable for second number of dice, either randomized or user's input
    int randomDice1 = (int) (Math.random() * 6)+1; // Declare/assign variable of 1st random number generated
    int randomDice2 = (int) (Math.random() * 6)+1; // Declare/assign variable of 2nd random number generated
    int score = 0; //declare score bc u will assign it l8r

    Scanner myScanner = new Scanner( System.in ); // Declare instance scanner 
    
    //Get user to input if they'd rather randomly cast dice or if they'd rather state the two dice they'd want to evaluate
    System.out.print("Type 1 if you would like to randomly cast dice. Type 2 if you'd like to state two dice you want to evaluate: "); 
    // Declare/assign variable of choice that they make -- either randomly casting dice or inputting two numbers
    int choice = myScanner.nextInt();
    
    switch (choice){
      case 1:  // declare the numbers of the dice as the random numbers generated if they choose choice 1
      number1 = randomDice1;
      number2 = randomDice2;
      int bothDice = (number1 * 10) + number2;
      score = number1 + number2;
      // Assign names to combinations of numbers
        switch(bothDice){
          case 11: 
            name = "Snake Eyes"; // Assign slang term of snake eyes if both dice are 1
          break;
          case 12:
          case 21: 
          name = "Ace Deuce"; // Assign slang term of ace deuce if one of the dice is a 1 and the other is a 2 
          break;
          case 13:
          case 31: 
          name = "Easy Four"; // assign slang term of easy four if one dice is 1 & other 3
          break;
          case 14:
          case 41: 
          name = "Fever Five";// assign slang term of fever 5 if one dice is 1 & other 4
          break;
          case 15: 
          case 51:
          name = "Easy Six"; // assign slang term of easy six if one dice is 1 & other 5
          break;
          case 16:
          case 61:
          name = "Seven Out"; // assign slang term of seven out if one dice is 1 & other 6
          break;
          case 22:
             name = "Hard Four"; // assign slang term of hard 4 if both dice are 2s
            break;
          case 23:
          case 32:
            name = "Fever Five"; // assign slang term of fever 5 if one dice is 2 & other 3
          break;
          case 24:
          case 42:
             name = "Easy Six"; // assign slang term of easy six if one dice is 2 & other 4
          break;
          case 25:
          case 52:
            name = "Seven Out"; // assign slang term of seven out if one dice is 2 & other 5
          break;
          case 26:
          case 62: 
            name = "Easy Eight"; // assign slang term of easy 8 if one dice is 2 & other 6
          break;
          case 33:
            name = "Hard Six"; // assign slang term of hard six if both dice is 6
          break;
          case 34:
          case 43:
            name = "Seven Out"; // assign slang term of seven out if one dice is 4 & other 3
          break;
          case 35:
          case 53:
            name = "Easy Eight"; // assign slang term of easy eight if one dice is 5 & other 3
          break;
          case 36:
          case 63:
          case 45:
          case 54:
            name = "Nine"; // assign slang term of easy four if dice add up to 9
          break;
          case 44:
            name = "Hard Eight"; // assign slang term of hard eight if both are 4s
          break;
          case 46:
          case 64:
            name = "Easy Ten"; // assign slang term of easy ten if one dice is 6 & other 4
          break;
          case 55:
            name = "Hard Ten"; // assign slang term of hard 10 if both dice are 5s
          break;
          case 56:
          case 65:
            name = "Yo-Leven"; // assign slang term of yo leven if one dice is 5 & other 6
          break;
          case 66:
            name = "Boxcars"; // assign slang term of box cars if both dice are 6s
          break;
      
        }
        System.out.println("You rolled a "+number1+" and a "+ number2+". Your score is "+score+" and named "+name );
        break;
      case 2:
       System.out.print("Type an integer from 1-6 for the first dice: ");
          int integer1 = myScanner.nextInt(); // Accept user's input
        switch(integer1){ // accept variable of first number if it is 1-6
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
          number1 = integer1; // Assign variable of number 1 to what user input
          System.out.print("Type an integer from 1-6 for the first dice: ");
          int integer2 = myScanner.nextInt(); 
            switch (integer2){ // accept variable if it's 1-6 
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
              case 6:
              number2 = integer2; // Assign variable of number2 to what user input
               bothDice = (number1 * 10) + number2; // combine both numbers into 1 number bc switch doesn't have OR
             score = number1 + number2; // create score for the two dice
            // Assign names to combinations of numbers
        switch(bothDice){
          case 11: 
            name = "Snake Eyes"; // Assign slang term of snake eyes if both dice are 1
          break;
          case 12:
          case 21: 
          name = "Ace Deuce"; // Assign slang term of ace deuce if one of the dice is a 1 and the other is a 2 
          break;
          case 13:
          case 31: 
          name = "Easy Four"; // assign slang term of easy four if one dice is 1 & other 3
          break;
          case 14:
          case 41: 
          name = "Fever Five";// assign slang term of fever 5 if one dice is 1 & other 4
          break;
          case 15: 
          case 51:
          name = "Easy Six"; // assign slang term of easy six if one dice is 1 & other 5
          break;
          case 16:
          case 61:
          name = "Seven Out"; // assign slang term of seven out if one dice is 1 & other 6
          break;
          case 22:
             name = "Hard Four"; // assign slang term of hard 4 if both dice are 2s
            break;
          case 23:
          case 32:
            name = "Fever Five"; // assign slang term of fever 5 if one dice is 2 & other 3
          break;
          case 24:
          case 42:
             name = "Easy Six"; // assign slang term of easy six if one dice is 2 & other 4
          break;
          case 25:
          case 52:
            name = "Seven Out"; // assign slang term of seven out if one dice is 2 & other 5
          break;
          case 26:
          case 62: 
            name = "Easy Eight"; // assign slang term of easy 8 if one dice is 2 & other 6
          break;
          case 33:
            name = "Hard Six"; // assign slang term of hard six if both dice is 6
          break;
          case 34:
          case 43:
            name = "Seven Out"; // assign slang term of seven out if one dice is 4 & other 3
          break;
          case 35:
          case 53:
            name = "Easy Eight"; // assign slang term of easy eight if one dice is 5 & other 3
          break;
          case 36:
          case 63:
          case 45:
          case 54:
            name = "Nine"; // assign slang term of easy four if dice add up to 9
          break;
          case 44:
            name = "Hard Eight"; // assign slang term of hard eight if both are 4s
          break;
          case 46:
          case 64:
            name = "Easy Ten"; // assign slang term of easy ten if one dice is 6 & other 4
          break;
          case 55:
            name = "Hard Ten"; // assign slang term of hard 10 if both dice are 5s
          break;
          case 56:
          case 65:
            name = "Yo-Leven"; // assign slang term of yo leven if one dice is 5 & other 6
          break;
          case 66:
            name = "Boxcars"; // assign slang term of box cars if both dice are 6s
          break; 
            }
             System.out.println("You rolled a "+number1+" and a "+ number2+". Your score is "+score+" and named "+name); // print score and dice numbers and names
             System.exit(0); //exit
              default: 
                System.out.println("Your input is out of range. Type an integer in the range of 1 to 6."); // tell them to print 2nd term in range
              System.exit(0); //exit
            }
       
            default:
            System.out.println("Your input is out of range. Type an integer in the range of 1 to 6."); // tell them to print 1st term in range(number1 == 1 && number2 == 1) {
            System.exit(0);//exit
        }
      default:
        System.out.println("Your input is out of range. Type an integer in the range of 1 to 2."); //tell them to put in a choice of 1 or 2
          System.exit(0);//exit
    }
  }
}