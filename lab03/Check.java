// Kristine Phan
// CSE 2 - 311
// 9/13/18
// split the check evenly and determine how much each person needs to spend (total and tip determined by user's inputs) 
// use Scanner class to obtain from the user the original cost of the check, the percentage tip they wish to pay,
// and the number of ways the check will be split

import java.util.Scanner; // import Scanner class to use it
// Add class and main method
public class Check{
  public static void main(String[] args) { 
  
    Scanner myScanner = new Scanner( System.in ); // Declare instance of scanner and construct instance of scanner; take input from STDIN 
  
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // Get user to input cost of check from user
    double checkCost = myScanner.nextDouble(); //Accept user input and assign variable of check cost
 
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) "); // Get user to input tip %
    double tipPercent = myScanner.nextDouble(); // Accept user input and assign variable of tip %
      tipPercent /= 100; // convert the percentage into a decimal value 
  
    System.out.print("Enter the number of people who went out to dinner: "); // Get user to input number of people that went to dinner
    int numPeople = myScanner.nextInt(); // Accept user input and assign variable of number of people who came
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;   //whole dollar amount of cost, and dimes and pennies for storing digits to the right of the decimal point 
    totalCost = checkCost * (1 + tipPercent); // assign variable of total cost of check, bill + tip
    costPerPerson = totalCost / numPeople; // divide entire check + bill by every person who attended
    dollars=(int)costPerPerson; //get the whole amount, dropping decimal fraction of cost per person 
    dimes=(int)(costPerPerson * 10) % 10;     //get dimes amount where the % (mod) operator returns the remainder from whole numbers (1st decimal place)
    pennies=(int)(costPerPerson * 100) % 10; // get pennies amount where the % (mod) operator returns the remainder from whole numbers (2nd decimal place)
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); // Print how much every person pays

  }
}

