// Kristine Phan
// 12/6/18
// lab 10 -- selection for worst case array and best case array 
import java.util.Arrays;
public class selection {
	public static void main(String[] args) {
	int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	int iterBest = selectionSort(myArrayBest);
  System.out.println("The total number of operations performed on the sorted array: " + iterBest);
	int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}
  /** The method for sorting the numbers */
	public static int selectionSort(int[] list) { 
		System.out.println(Arrays.toString(list)); // Prints the initial array 
		int iterations = 0; // Initialize counter for iterations

		for (int i = 0; i < list.length - 1; i++) {
			iterations++;// Update the iterations counter
			int currentMin = list[i]; // Step One: Find the minimum in the list[i..list.length-1]
			int currentMinIndex = i;
			for (int j = i + 1; j < list.length; j++) { 
        i++;
				if (list[i] > list[j]) {
          currentMin = list[j];
          currentMinIndex = j;
          }
        
				iterations++; // increase iteration counter every time you compare the current min to another element in the array.)
			}
			// Step Two: Swap list[i] with the minimum you found above
			if (currentMinIndex != i) { // if the min value does not equal the current value we are on 
				int temp = list[i];
        list[i] = list[currentMinIndex];
        list[currentMinIndex] = temp;
        System.out.println(Arrays.toString(list));
			}
    }
		return iterations;
	
}
}

