// Kristine Phan
// lab9
// In your main method, declare a literal integer array of length at least 8 and make two copies of it using the copy() method, 
// called array0, array1, array2.
// Pass array0 to inverter() and print it out with print().  Do not assign the output of inverter to anything.
//Pass array1 to inverter2() and print it out with print().  Do not assign the output of inverter to anything.
//Pass array2 to inverter2() and assign the output to array3.  Array3 does not require an allocation, just declaration.  Print out array3 with print().

import java.util.Scanner; 

public class PassingArraysInsideMethods{
  
  
  public static int[] copy(int[] list){
   int[] array = new int[list.length];

    for (int i=0; i<list.length; i++){
	    array[i] = list[i];
  }
  return array;
  }
    
  public static void inverter(int [] list0){
  for (int i=0; i<list0.length/2; i++){
     int temp = list0[i];
     list0[i] = list0[list0.length-1-i];
     list0[list0.length-1-i] = temp;
  }
  }

  public static int[] inverter2(int [] list0){
  int[] list = copy(list0);
  for (int i=0; i<list.length/2; i++){
     int temp = list[i];
     list[i] = list[list.length-1-i];
     list[list.length-1-i] = temp;
  }
    return list;
  }
  
    public static void print(int[] arrays){
      for (int i = 0; i<arrays.length; i++){
        System.out.print(arrays[i]);
      }
              System.out.println();

    }
  
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    int[] array0 =  {8, 7, 6, 5, 4, 3, 2, 1};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    print(array3);
  }
}