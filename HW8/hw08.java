// Kristine Phan
// 11/11/18
// Shuffling 
// write your own code for the three methods called shuffle(list), getHand(list,index, numCards), and printArray(list)
// given a deck of 52 cards, represented by the array cards of Strings
// You will have to print out all the cards in the deck, shuffle the whole deck of cards, then print out the cards in the deck,
// all shuffled, then getting a hand of cards and print them out
// The method printArray(list) takes an array of Strings and prints out each element, separated by a space “ “
// shuffle(list) shuffles the elements of the list by continuously randomize an index number of list (that is not zero) and 
//swaps the element at that index with the first element (at index 0). 
//Swap however many times you like, but choose > 50 times since you want the deck of 52 cards well shuffled.
//getHand(list, index, numCards) returns an array that holds the number of cards specified in numCards
// Cards should be taken off at the end of the list of cards. 
// For example, when this method is first invoked where numCards = 2, 
// the card at index 51 and index 50 should be returned within an array.
// At the second invocation of getHand() the index value passed into getHand should be 49.  
// Note the index passed into this method is the last index value of the array. Index should change in the main method. 
//If numCards is greater than the number of cards in the deck, create a new deck of cards.
import java.util.Scanner; // import scanner

public class hw08{ 
  
  public static String[] getHand(String[] list, int index, int numCards){ // make method for generating hands
    if (index+1 < numCards){//if the amount of cards left in the deck are less than the amount of cards for a new set of hands,
      index = 51; // set index to 51 (end of new deck)
      shuffle(list);//shuffle new deck
    }
    String[] hand = new String[numCards]; //initialize array of hands of 5 cards
    for(int i=0; i<numCards; i++){ 
      hand[i] = list[index-i]; //make an array of 5 cards starting from last card in shuffled deck and then from next last 5 if they ask for another set of hands
    }
    return hand; // return array of set of hands
  }  
  
  
public static String[] shuffle(String[] list){ //make method for shuffling cards
/*
 for(int i=0; i< 52; i++){ // swap cards 52 times
  String temp = list[i]; // set value of index 0 as temporary value
    int swap = (int) Math.random()*52; // generate random index
    list[i] = list[swap]; // set index 0 to value of randon index
    list[swap] = temp; // set index of random index to index 0's value
 }
 */
  String temp; // initialize value of index 0 (2C)
  for(int i = 0; i < 100; i++){ //do swap 100 times
   temp = list[0];
   // System.out.print(temp);
  int swap = (int)(Math.random()*51)+1; // initialize index of random card
  // System.out.print(swap);
  list[0] = list[swap]; // swap index 0 with card of random index
  list[swap] = temp;  
  }
 
  return list; //return array of shuffled deck as new value of cards
 }


public static void printArray(String[] list){ // print array of shuffled deck or hands
  for (int i = 0; i<list.length; i++){ // print each value of cards until number of cards wanted (52 for deck, 5 for hands)
    System.out.print(list[i]+" ");
  }
  System.out.println(); //go to next line
}


public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  //System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
 hand = getHand(cards,index,numCards); 
 printArray(hand);
   index = (index - numCards);
  if(index < 0){ //if index becomes negative
    index = 51-numCards; // set index to 51-number of cards of hands bc due to pass of values, it already generated hands of new deck but didn't save that the index changed
  }
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 
}