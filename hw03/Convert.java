// Kristine Phan
// CSE 2 311
// 9/18/18
// Program #1 that asks  user for doubles that represent  number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average in mi^3

import java.util.Scanner; // import Scanner class to use it

// Add class and main method
public class Convert{
  public static void main(String[] args) { 
    
    Scanner myScanner = new Scanner( System.in ); // Declare instance of scanner and construct instance of scanner; take input from STDIN 
   
    // Take inputs from users and assign variables
    System.out.print("Enter the number of acres of land affected by hurricane precipitation including its decimals: "); // Get user to input amount of acres of land that rain falls on
    double acres = myScanner.nextDouble(); //Accept user input and assign variable of acres of land 
    
    System.out.print("Enter how many inches of rain were dropped on average: "); // Get user to input amount of average rainfall in inches
    double rainInches = myScanner.nextDouble(); // Accept user input and assign variable of rainfall in inches
    
    // Assign variables of conversion (gallons, cubic miles)
    double gallonsConversion = 27154; // 1 inch * 1 acre = 27154 gallons >> use to convert inches and acres from user to gallons
    double cubicMilesConversion = 9.08169 * Math.pow(10,-13); // 1 gallon = 9.08169 * 10^-13 cubic miles >> use to convert the gallons to cubic miles
    
    // Run calculations
    double gallons = rainInches * acres * gallonsConversion; // Take input of average inches of rain and amount of acres rain falls on, and convert to gallons
    double cubicMiles = gallons * cubicMilesConversion; // Convert gallons of rain on acres to cubic miles
    
    System.out.println(+ cubicMiles +" cubic miles "); // Print how many cubic miles of rain that the hurricane produced
    
  }
}

      