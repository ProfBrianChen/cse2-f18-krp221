// Kristine Phan
// CSE 2 311
// 9/18/18
// Program #2 that prompts  user for the dimensions of a pyramid and returns volume inside the pyramid

import java.util.Scanner; //import Scanner

public class Pyramid{
  public static void main (String[] args){
    
    Scanner myScanner = new Scanner( System.in ); // Declare instance of scanner and construct instance of scanner
    
    // Take inputs from user and assign variables
    System.out.print("The square side of the pyramid is (input length/width): "); // Get input for square length of pyramid, which is the same as width because it's a square
    double length = myScanner.nextDouble(); // Accept input for square length/width of pyramid
    
    System.out.print("The height of the pyramid is (input height): "); // Get input for height of pyramid 
    double height = myScanner.nextDouble(); //Accept input for height of pyramid 
    
    // Run calculations
    double totalVolume = (length * length * height)/3; // Calculate volume with pyramid volume formula (lwh/3) where length and width are equal bc it's a square
    System.out.println("The volume inside the pyramid is: " + totalVolume); // Print total volume
}
}
