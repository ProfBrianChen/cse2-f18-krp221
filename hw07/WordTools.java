// Kristine Phan
// CSE2 311
// 10/27/18
// implement  sampleText() method. store text in string. print string 
// Implement a printMenu() method, which outputs a menu of user options for analyzing/editing the string, 
// and returns the user's entered menu option. Each option is represented by a single character.
// If an invalid character is entered, continue to prompt for a valid choice.
// Hint: Implement Quit before implementing other options. Call printMenu() in the main() method. 
// In the main menu, continue to call printMenu() each time the user enters an incorrect input, after each time the user enters an acceptable input, until the user enters q to Quit. Call the corresponding method after users enter acceptable inputs.


import java.util.Scanner; // import scanner

public class WordTools{
  
  public static String sampleText(){ // implement sampleText() method
    Scanner scan = new Scanner( System.in ); // declare scanner
    System.out.println("Enter a text: "); // tell user to input sample text
    
    String sampleText = scan.nextLine(); // declare and assign sample text as user's input
        System.out.println("You entered:"); // reiterate what they inputted
    System.out.println(sampleText); // print sample text
    return sampleText; // store sample text 
  }
  
  public static void printMenu(){
    // make menu as a string
  String menu = ("MENU \n c - Number of non-whitespace characters \n w - Number of words \n f - Find text \n r - Replace all !'s \n s - Shorten spaces\n q - Quit \n Choose an option: ");
  System.out.println(menu); // store menu to call

  }
  
  public static int getNumOfWords(String text){ // run number of words with an input of text
      int i;             // Current index in text
      int words = 0; // initialize number of spaces

     for (i = 0; i < text.length() - 1 ; i++) { // run until the end of text 
         if (Character.isLetter(text.charAt(i)) && text.charAt(i+1) == ' '){
           words++; // increase count for spaces for when there are spaces
         }
  }    return (words+1); // return number of words = number of spaces + 1

  }
  public static int getNumOfNonWSCharacters(String text){ // run number of non white space characters
       int i;             // Current index in text
      int letters = 0; // initialize number of non white space characters

     for (i = 0; i < text.length(); i++) {  // run until end of text length
         if (text.charAt(i) != ' '){ 
           letters++; // increase count of letters for every letter that is not a space
         }
  }
        return (letters); // return number of non white space characters

  }
 public static int findText(String text, String search){ // run number of times searched word is in text

  int count = 0;//initialize counter  
   int i= 0; // initialie other counter through string stepper
   String phrases = " ";
   for (i=0; i <= text.length() - search.length(); i++){ //run in sections of searchword's length until the last section before word ends
   phrases = text.substring(i,i+search.length()); // phrases are the words/phrases that begin at every letter (until the last section) and last until the end of the searchword's length
   //  System.out.println("Here is phrases: " + phrases);
     if (phrases.equals(search)){
       count++; // if the phrases of every section are the same as the keyword, increase count
     }
     }
          return count;
 }

    public static String replaceExclamation(String text){ // run text but with periods instead of exclam
      int i; // index
      char letters; // initialize letters in string
      String words = " "; // initialize words
     for (i = 0; i < text.length(); i++){ //run until end of sample text
       if (text.charAt(i) != '!'){ // keep characters same if not exclam
        letters = text.charAt(i);
         words += letters; // add letters and symbols onto string returned
       }
       else {
         letters = '.'; // if exclam change ! to .
         words += letters; // add . to string of words
             }
     }
      return words; // return edited text
    }
  
  public static String shortenSpace(String text){
    while (text.indexOf("  ") != -1) { // run only if double space exists
         text = text.replace("  ", " "); //call replace method to replace double space with 1
      }
  return text;
}
    
  public static void main(String[] args){
       Scanner scan = new Scanner( System.in );

    String text = sampleText(); //call sample text
    String input = " ";//initialiE input
    boolean correct = false; //initialize boolean
    String search = " "; //initialize the search for command f
   
       
printMenu(); // call print menu
    input = scan.next();// take in input
    
  while (!correct){ //run until user presses q
    if (input.equals("q")){
      correct = true;
  }
  else if (input.equals("w")){ 
            int words = getNumOfWords(text); //call getnumofwords
    System.out.println("Number of words: " + words); // if user presses w, call method and print number of words
      printMenu(); // call print menu
        input = scan.next();

  }
    else if (input.equals("c")){ //if user presses c call method and print number of non white space symbols
         int letters = getNumOfNonWSCharacters(text); //call number of non white space characters
      System.out.println("Number of non-white spaces: "+ letters);
      printMenu(); // call print menu
      input = scan.next();
    }
   else if (input.equals("f")){
   System.out.println("Enter a word or phrase to be found"); //ask user for a phrase
         search = scan.next(); // take user's input of the word to find
         int found = findText(text, search); // initialize and call findText
      System.out.println("instances: "+found);
    printMenu(); // call print menu
      input = scan.next();
       }
       
    else if (input.equals("r")){
          String replace = replaceExclamation(text); //call replace exclamations
      System.out.println("Edited text: "+replace); // print edited text
  printMenu(); // call print menu
      input = scan.next(); //take user's input
    }
    else if (input.equals("s")){
      String shortened = shortenSpace(text); //call replace shorten spaces
      System.out.println("Edited text: "+shortened); //print edited text
      printMenu(); // call print menu
      input = scan.next(); //take user's input
    }
    else {
printMenu(); // call print menu
      input = scan.next(); //take user's input
    }
    }
    }

}



 